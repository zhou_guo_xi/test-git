

import React from 'react';

const MyComponent = () => {
    return (
        <div>
            <h1>Hello from test-merge!</h1>
        </div>
    );
};

export default MyComponent;